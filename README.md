# Unity Asset Preprocessor
A tool for your unity project to automate texture and model imports and related settings.

![Preview](https://i.imgur.com/Hi0DIZ6.png)

## Features
Here's what the script can set up for the asset every time you import a model or a texture.

#### Models:
 * BlendShape normals
 * Tangent calculation/import algorithm
 * Camera imports
 * Quad retention

#### Textures:
 * MIP-maps generation
   * MIP-map streaming
 * AlphaTransparency flagging
 * Default texture size
 * Default texture compression
 * Crunch compression
 * Set texture to Normal Map type based on suffix input
 * Linearize colours based on suffix input (disables sRGB option)
 * Set texture to Single Channel type with selectable target channel based on suffix input
 * Ignore textures starting with a specific prefix

Suffix-filtering takes the last bit of the filename separated by underscores (`_`), then splits that bit by periods (`.`) and takes the last element. This allows for complex suffix naming. The only thing you should make sure of is that your container name is the last bit of the filename.

For instance, names `Tx_col` and `Tx_whatever_whenever_a.bc.d.e.fgh.col` equally resolve to `col` filter.

## Installation
Simply [grab the latest release](https://gitlab.com/Rikketh/Unity-AssetPreprocessor/-/releases) or download the repo, then throw `AssetPreprocessor` folder somewhere into your project's `Assets` folder. If everything's fine, you should see a new item under the "Tools" dropdown menu at the top of Unity editor called `Asset Preprocessor Settings`.

# Credits
This is a rework of [Hugo Zink](https://github.com/HugoZink)'s [UnityVRAssetImporter](https://github.com/HugoZink/UnityVRAssetPreImporter), thus obligatory links to precursors.